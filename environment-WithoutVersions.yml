################################################################################
###                                                                          ###
### Created by Martin Genet for MEC581                                       ###
###                                                                          ###
### École Polytechnique, Palaiseau, France                                   ###
###                                                                          ###
################################################################################

name: MEC581

channels:
  - conda-forge
  - defaults

dependencies:
# python
  - python
  - mpi4py=3.1.3 # MG20220906: To avoid mpi warnings in FEniCS, cf. https://fenicsproject.discourse.group/t/runtimewarning-mpi4py-mpi-file-size-changed-may-indicate-binary-incompatibility-expected-32-from-c-header-got-40-from-pyobject-def-compile-class-cpp-data-mpi-comm-mpi-comm-world/6496 # MG20220906: Note that 3.0.3 leads to a `RuntimeError: Error when importing mpi4py`, but 3.1.3 seems to work.
  - numpy=1.21.5 # MG20221220: later numpy versions seem incompatible with itkwidgets 0.32
  - scipy
  - sympy=1.9  # MG20220906: There is a problem with matrix differentiation with sympy >= 1.10, cf. https://github.com/sympy/sympy/issues/24021
  - traitlets=5.6.0 # MG20221220: later traitlets versions seem incompatible with itkwidgets 0.32, cf. https://github.com/InsightSoftwareConsortium/itkwidgets/issues/588

# jupyter
  - ipywidgets=7.6.5 # MG20221226: cf. https://github.com/InsightSoftwareConsortium/itkwidgets/pull/590/files
  - itkwidgets=0.32.0 # MG20221220: later versions do not support meshes, cf. https://github.com/InsightSoftwareConsortium/itkwidgets/issues/520
  - jupyter
  - jupyter_contrib_nbextensions
  - jupyter_nbextensions_configurator
  - jupyterlab
  - notebook=6.4.6 # MG20221226: cf. https://github.com/InsightSoftwareConsortium/itkwidgets/pull/590/files

# coding stuff
  - git

# computing stuff
  - fenics=2019.1.0
  - gmsh
  - itk
  - meshio
  - vtk=9.0.3 # MG20221220: Apparently there is some kind of conflict between vtk9.1 & itkwidgets 0.32… # MG20220628: Apparently there is some kind of conflict between vtk9.1 & libstdc++.so.6 in Ubuntu 20.04… # MG20201223: Need to correspond to the environment variable CPATH

# plotting stuff
  - matplotlib
  - gnuplot

  - pip
  - pip:
# jupyter
    - itkwidgets

# computing stuff
    - git+https://github.com/dolfin-adjoint/pyadjoint.git@2019.1.0
    - gmsh

# tracking stuff
    - dolfin_warp

# variables:
    # CPATH: /opt/miniconda3/envs/MEC581/include/vtk-9.0 # MG20201223: Need to correspond to the installed version of VTK
    # CPATH: /srv/conda/envs/notebook/include/vtk-9.0 # MG20201213: Path for Binder
    # CPATH: ~/.conda/envs/MEC581/include/vtk-9.0 # MG20210103: Path for Salles Infos
